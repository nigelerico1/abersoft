import 'dart:io';
import 'package:abersoft/main_app.dart';
import 'package:abersoft/ui/splash_page.dart';
import 'package:abersoft/utils/constant_helper.dart';
import 'package:abersoft/utils/show_flutter_toast.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App {
  static App? _instance;
  final String? apiBaseURL;
  final String? appTitle;

  SharedPreferences? prefs;
  Dio? dio;
  Directory? appDocsDir;

  App.configure({
    this.apiBaseURL,
    this.appTitle
  }) {
    _instance = this;
  }

  factory App() {
    if (_instance == null) {
      throw UnimplementedError("App must be configured first.");
    }

    return _instance!;
  }

  Future<void> init() async {

    prefs = await SharedPreferences.getInstance();

    appDocsDir = await getApplicationDocumentsDirectory();

    dio = Dio(BaseOptions(
      baseUrl: apiBaseURL!,
      connectTimeout: 10000,
      receiveTimeout: 50000,
      contentType: Headers.jsonContentType,
      responseType: ResponseType.json,
    ));

    dio!.options.headers = {
      'Authorization': 'Bearer ${prefs!.get(ConstantHelper.PREFS_TOKEN_KEY)}',
    };


    (dio!.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (HttpClient client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      return client;
    };

    dio!.interceptors.add(InterceptorsWrapper(
        onRequest:(options, handler){

          return handler.next(options); //continue

        },
        onResponse:(response,handler) {

          return handler.next(response); // continue

        },
        onError: (DioError e, handler) {

          if (e.response!.statusCode != null) {
            if (e.response!.statusCode == 400) {
            }
            // INFO : Kicking out user to login page when !authenticated
            if (e.response!.statusCode == 401) {
              prefs!.setBool(ConstantHelper.PREFS_IS_USER_LOGGED_IN, false);
              prefs!.setString(ConstantHelper.PREFS_TOKEN_KEY, "");
              showFlutterToast('Silahkan login kembali');
              NavigatorState? navigatorState = MainApp.navigatorKey.currentState;
              if (navigatorState != null) {
                navigatorState.pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => const SplashPage()),
                        (route) => route == null);
              }

            }
          }
          return  handler.next(e);//continue

        }
    ));


  }

}