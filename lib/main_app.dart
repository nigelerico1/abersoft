import 'package:abersoft/app.dart';
import 'package:abersoft/cubit/add/add_cubit.dart';
import 'package:abersoft/cubit/auth/auth_cubit.dart';
import 'package:abersoft/cubit/home/home_cubit.dart';
import 'package:abersoft/ui/splash_page.dart';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class MainApp extends StatelessWidget {
  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  const MainApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthCubit authCubit = AuthCubit();
    HomeCubit homeCubit = HomeCubit();
    AddCubit addCubit = AddCubit();

    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthCubit>(create: (context) => authCubit),
        BlocProvider<HomeCubit>(create: (context) => homeCubit),
        BlocProvider<AddCubit>(create: (context) => addCubit),
      ],
      child: ScreenUtilInit(
        designSize: const Size(360, 690),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          return MaterialApp(
              navigatorKey: navigatorKey,
              debugShowCheckedModeBanner: false,
              title: App().appTitle!,
              theme: ThemeData(
                appBarTheme: const AppBarTheme(
                  color: AbersoftColors.colorPrimary,
                ),
                primaryColor: AbersoftColors.colorPrimary,
                primaryColorDark: AbersoftColors.colorPrimary,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                brightness: Brightness.light,
                textSelectionTheme: const TextSelectionThemeData(
                  cursorColor: Colors.black,
                  selectionColor: AbersoftColors.colorPrimary,
                  selectionHandleColor: AbersoftColors.colorPrimary,
                ),
              ),
              home: const SplashPage()
          );
        },
      ),
    );
  }
}
