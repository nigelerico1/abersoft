import 'dart:io';

import 'package:abersoft/cubit/add/add_cubit.dart';
import 'package:abersoft/models/add/add_request.dart';
import 'package:abersoft/ui/widgets/base/button.dart';
import 'package:abersoft/ui/widgets/base/custom_textfield.dart';
import 'package:abersoft/ui/widgets/base/space.dart';
import 'package:abersoft/ui/widgets/modules/app_alert_dialog.dart';
import 'package:abersoft/ui/widgets/modules/loading_dialog.dart';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:abersoft/utils/show_flutter_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';

class AddPage extends StatefulWidget {
  const AddPage({super.key});

  @override
  State<AddPage> createState() => _AddPageState();
}

class _AddPageState extends State<AddPage> {
  final ImagePicker _picker = ImagePicker();
  XFile? imagePicker;
  late AddCubit _addCubit;
  final productNameTextController = TextEditingController();
  final productDescriptionTextController = TextEditingController();

  void pickPhoto() async {
    XFile? image =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 25);
    setState(() {
      imagePicker = image;
    });
  }

  @override
  void initState() {
    _addCubit = BlocProvider.of<AddCubit>(context);
    super.initState();
  }

  void capturePhoto() async {
    XFile? image =
        await _picker.pickImage(source: ImageSource.camera, imageQuality: 25);
    setState(() {
      imagePicker = image;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddCubit, AddState>(
      bloc: _addCubit,
      listener: (context, state) {
        if (state is AddProductInitialState) {
          LoadingDialog(title: 'Loading', description: 'Please wait...')
              .show(context);
        } else if (state is AddProductSuccessfulState) {
          Navigator.pop(context);
          Navigator.pop(context);
          showFlutterToast(state.message ?? "Terjadi Kesalahan");
        } else if (state is AddProductFailedState) {
          Navigator.pop(context);
          AppAlertDialog(
              title: 'Add Product Failed',
              description: 'Try again',
              positiveButtonText: 'Ok',
              positiveButtonOnTap: () => Navigator.pop(context)).show(context);
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: false,
          backgroundColor: Colors.white,
          title: const Text(
            "Add Products",
            style: TextStyle(color: Colors.black),
          ),
          leading: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(16),
            child: Column(
              children: [
                const Space(height: 20),
                Container(
                  height: 350.h,
                  width: 280.w,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    border: Border.all(color: Colors.grey),
                  ),
                  child: imagePicker == null
                      ? GestureDetector(
                          onTap: () {
                            capturePhoto();
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Align(
                                alignment: Alignment.center,
                                child: Image.asset(
                                  'assets/images/camera.png',
                                  fit: BoxFit.cover,
                                  width: 125,
                                  height: 125,
                                ),
                              ),
                              const Text(
                                "Take Picture",
                                style: TextStyle(
                                    fontSize: 18, color: Colors.black),
                              )
                            ],
                          ),
                        )
                      : Image.file(
                          File(imagePicker!.path),
                          fit: BoxFit.contain,
                        ),
                ),
                const Space(height: 30),
                CustomTextField(
                  controller: productNameTextController,
                  title: "Product Name",
                  titleFontWeight: FontWeight.w600,
                  placeholder: "Enter Product Name",
                  size: 14,
                  keyboardType: TextInputType.text,
                ),
                CustomTextField(
                  title: "Product Description",
                  controller: productDescriptionTextController,
                  titleFontWeight: FontWeight.w600,
                  placeholder: "Enter Product Description",
                  size: 16,
                  keyboardType: TextInputType.text,
                  maxLines: 4,
                ),
                Button(
                    faIcon: FontAwesomeIcons.images,
                    style: AppButtonStyle.fourth,
                    colorIcon: Colors.black,
                    height: 52,
                    radiusCircular: 10,
                    fontSize: 16,
                    padding: 8,
                    colorsText: Colors.black,
                    fontWeight: FontWeight.w700,
                    text: 'Select Image From Gallery',
                    onPressed: () {
                      pickPhoto();
                    }),
                const Space(height: 20),
                Button(
                    style: AppButtonStyle.primary,
                    height: 52,
                    radiusCircular: 10,
                    fontSize: 16,
                    padding: 8,
                    fontWeight: FontWeight.w700,
                    text: 'Save',
                    onPressed: () {
                      if (imagePicker != null) {
                        AddRequest addRequest = AddRequest();
                        addRequest.productImage = imagePicker;
                        addRequest.productName = productNameTextController.text;
                        addRequest.productDecription =
                            productDescriptionTextController.text;

                        _addCubit.addProduct(addRequest);
                      } else {
                        showFlutterToast("Select Photo First");
                      }
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }
}
