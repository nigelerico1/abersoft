import 'package:abersoft/cubit/auth/auth_cubit.dart';
import 'package:abersoft/cubit/home/home_cubit.dart';
import 'package:abersoft/models/auth/login_request.dart';
import 'package:abersoft/ui/pages/home/navigator_page.dart';
import 'package:abersoft/ui/widgets/base/button.dart';
import 'package:abersoft/ui/widgets/base/custom_textfield.dart';
import 'package:abersoft/ui/widgets/base/space.dart';
import 'package:abersoft/ui/widgets/modules/app_alert_dialog.dart';
import 'package:abersoft/ui/widgets/modules/loading_dialog.dart';
import 'package:abersoft/utils/global_method_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late AuthCubit _authCubit;
  late HomeCubit _homeCubit;

  final usernameTextController = TextEditingController();
  final passwordTextController = TextEditingController();

  @override
  void initState() {
    _authCubit = BlocProvider.of<AuthCubit>(context);
    _homeCubit = BlocProvider.of<HomeCubit>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthCubit, AuthState>(
      listener: (context, state) {
        if (state is LoginInitialState) {
          LoadingDialog(
              title: 'Loading',
              description: 'Please wait...'
          ).show(context);
        } else if (state is LoginSuccessfulState) {
          _homeCubit.changeSelectedPage(0);
          Navigator.pop(context);
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => const NavigatorPage()),
                  (Route<dynamic> route) => false);

        } else if (state is LoginFailedState) {
          Navigator.pop(context);
          AppAlertDialog(
              title: 'Login Gagal',
              description: 'Username atau password yang anda masukkan salah',
              positiveButtonText: 'Ok',
              positiveButtonOnTap: () => Navigator.pop(context)
          ).show(context);
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Container(
            padding: const EdgeInsets.all(16),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Space(
                    height: 21,
                  ),
                  Image.asset(
                    'assets/images/LOGO.png',
                    fit: BoxFit.fitWidth,
                    width: ScreenUtil().setWidth(288),
                    height: ScreenUtil().setHeight(80),
                  ),
                  const Space(
                    height: 55,
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Enter username and password",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 16),
                          ),
                          const Space(
                            height: 18,
                          ),
                          CustomTextField(
                            keyboardType: TextInputType.text,
                            placeholder: "Enter Username",
                            autoFocus: true,
                            controller: usernameTextController,
                            radius: 20,
                            maxLines: 1,
                          ),
                          const Space(
                            height: 9,
                          ),
                          CustomTextField(
                            keyboardType: TextInputType.text,
                            placeholder: "Enter Password",
                            controller: passwordTextController,
                            radius: 20,
                            obscureText: true,
                            maxLines: 1,
                          ),
                          const Space(
                            height: 9,
                          ),
                          Button(
                            style: AppButtonStyle.primary,
                            radiusCircular: 20,
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                            text: 'Login',
                            padding: 14,
                            onPressed: () {
                              if (!GlobalMethodHelper.isEmpty(
                                      usernameTextController.text) &&
                                  !GlobalMethodHelper.isEmpty(
                                      passwordTextController.text)) {
                                _authCubit.loginUser(LoginRequest(
                                    username: usernameTextController.text,
                                    password: passwordTextController.text));
                              }
                            },
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
