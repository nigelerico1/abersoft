import 'package:abersoft/models/product/product_model.dart';
import 'package:abersoft/ui/pages/home/detail_product_page.dart';
import 'package:abersoft/ui/widgets/base/space.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListProducts extends StatelessWidget {
  final AllProduct data;
  const ListProducts({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => DetailProductPage(data: data)));
      },
      child: Container(
        width: 170,
        margin: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        decoration: const BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 6.0,
              ),
            ],
            borderRadius: BorderRadius.all(Radius.circular(16))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(16.0),
                topRight: Radius.circular(16.0),
              ),
              child: CachedNetworkImage(
                height: 100,
                width: 170,
                fit: BoxFit.cover,
                imageUrl: data.imageUrl ?? "",
                errorWidget: (context, url, error) =>
                const Icon(Icons.error),
              ),
            ),
            const SizedBox(height: 12,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: Text(data.name ?? ""),
            )
          ],
        ),
      ),
    );
  }
}
