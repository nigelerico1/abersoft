import 'package:abersoft/models/product/product_model.dart';
import 'package:abersoft/ui/widgets/base/space.dart';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class DetailProductPage extends StatelessWidget {
  final AllProduct data;

  const DetailProductPage({super.key, required this.data});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Detail Products",
          style: TextStyle(color: Colors.black),
        ),
        leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: SizedBox(
                height: 250,
                child: CachedNetworkImage(
                  fit: BoxFit.fill,
                  imageUrl: data.imageUrl ?? "",
                  errorWidget: (context, url, error) => const Icon(Icons.error),
                ),
              ),
            ),
            const Space(
              height: 16,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(data.name ?? "",
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: Colors.black)),
            ),
            const Space(
              height: 16,
            ),
            Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(data.productDescription ?? "",
                    style: const TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: AbersoftColors.colorPrimary))),
          ],
        ),
      ),
    );
  }
}
