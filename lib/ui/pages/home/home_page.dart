import 'package:abersoft/cubit/home/home_cubit.dart';
import 'package:abersoft/models/product/product_model.dart';
import 'package:abersoft/ui/pages/add/add_page.dart';
import 'package:abersoft/ui/pages/home/widgets/list_best_products.dart';
import 'package:abersoft/ui/widgets/base/space.dart';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:abersoft/utils/show_flutter_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late HomeCubit _homeCubit;

  @override
  void initState() {
    _homeCubit = BlocProvider.of<HomeCubit>(context);
    _homeCubit.getListProducts();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeCubit, HomeState>(
      bloc: _homeCubit,
      listener: (context, state) {
        if (state is GetProductsFailedState) {
          showFlutterToast(state.message ?? "Terjadi Kesalahan");
        }
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          // title: Text("Dashboard", style: TextStyle(color: Colors.black),),
          title: const Text(
            "Home",
            style: TextStyle(color: Colors.black),
          ),
          actions: <Widget>[
            Container(
              padding: const EdgeInsets.only(right: 10.0),
              width: ScreenUtil().setWidth(90),
              height: ScreenUtil().setHeight(30),
              child: const Image(
                image: AssetImage(
                  'assets/images/LOGO.png',
                ),
                fit: BoxFit.contain,
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.only(bottom: 60),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text("Best Products", style: TextStyle(color: Colors.black, fontSize: 16),),
                const Space(height: 10,),
                BlocBuilder<HomeCubit, HomeState>(
                  bloc: _homeCubit,
                  builder: (context, state) {
                    if (state is GetProductsInitialState) {
                      return Center(
                        child: LoadingAnimationWidget.twistingDots(
                          leftDotColor: const Color(0xFF1A1A3F),
                          rightDotColor: AbersoftColors.colorPrimary,
                          size: 40,
                        ),
                      );
                    } else if (state is GetProductsSuccessfulState) {
                      return SizedBox(
                        height: 170,
                        child: ListView.builder(
                            physics: const ClampingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                            itemCount: state.bestProducts.length,
                            itemBuilder: (context, int index) {
                              AllProduct data = state.bestProducts[index];
                              return ListProducts(data: data,);
                            }),
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
                const Space(height: 14,),
                const Text("All Products", style: TextStyle(color: Colors.black, fontSize: 16),),
                const Space(height: 10,),
                BlocBuilder<HomeCubit, HomeState>(
                  bloc: _homeCubit,
                  builder: (context, state) {
                    if (state is GetProductsInitialState) {
                      return Center(
                        child: LoadingAnimationWidget.twistingDots(
                          leftDotColor: const Color(0xFF1A1A3F),
                          rightDotColor: AbersoftColors.colorPrimary,
                          size: 40,
                        ),
                      );
                    } else if (state is GetProductsSuccessfulState) {
                      return GridView.builder(
                          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              childAspectRatio: 1.1),
                          physics: const NeverScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: state.allProducts.length,
                          itemBuilder: (context, int index) {
                            AllProduct data = state.allProducts[index];
                            return ListProducts(data: data,);
                          });
                    } else {
                      return Container();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const AddPage()));
          },
          backgroundColor: AbersoftColors.colorPrimary,
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
