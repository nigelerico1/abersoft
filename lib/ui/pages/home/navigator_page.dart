import 'package:abersoft/cubit/home/home_cubit.dart';
import 'package:abersoft/ui/pages/home/home_page.dart';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../profile/profile_page.dart';

class NavigatorPage extends StatefulWidget {
  const NavigatorPage({super.key});

  @override
  State<NavigatorPage> createState() => _NavigatorPageState();
}

class _NavigatorPageState extends State<NavigatorPage> {

  late HomeCubit _homeCubit;
  late List<Widget> _pages;

  @override
  void initState() {
    _homeCubit = BlocProvider.of<HomeCubit>(context);
    _pages = _getPages();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      bloc: _homeCubit,
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () async {
            if (_homeCubit.selectedPage != 0) {
              _homeCubit.changeSelectedPage(0);
              return false;
            } else {
              return true;
            }
          },
          child: Scaffold(
            body: _pages[_homeCubit.selectedPage],
            bottomNavigationBar: Container(
              decoration:
              const BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                  color: Color(0x1A000000),
                  offset: Offset(0, -3),
                  blurRadius: 20,
                ),
              ]),
              child: BottomNavigationBar(
                currentIndex: _homeCubit.selectedPage,
                type: BottomNavigationBarType.fixed,
                showUnselectedLabels: true,
                showSelectedLabels: true,
                items: _getMenuItems(),
                selectedLabelStyle: const TextStyle(fontWeight: FontWeight.w600, color: Colors.black),
                unselectedItemColor: AbersoftColors.colorGrey,
                selectedItemColor: AbersoftColors.colorPrimary,
                onTap: (int index) => _homeCubit.changeSelectedPage(index),
              ),
            ),
          ),
        );
      },
    );
  }

  List<Widget> _getPages() {
    List<Widget> pages = [
      const HomePage(),
      const ProfilePage(),
    ];

    return pages;
  }

  List<BottomNavigationBarItem> _getMenuItems() {
    List<BottomNavigationBarItem> mn = [
      const BottomNavigationBarItem(
        icon: Icon(Icons.home),
        label: 'Home',
      ),
      const BottomNavigationBarItem(
          icon: Icon(Icons.person),
          label: 'Profile'
      ),
    ];


    return mn;
  }

}
