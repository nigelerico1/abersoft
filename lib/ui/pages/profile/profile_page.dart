import 'package:abersoft/app.dart';
import 'package:abersoft/ui/pages/auth/login_page.dart';
import 'package:abersoft/ui/widgets/base/button.dart';
import 'package:abersoft/ui/widgets/modules/app_alert_dialog.dart';
import 'package:abersoft/utils/constant_helper.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Button(
          style: AppButtonStyle.primary,
          radiusCircular: 20,
          fontSize: 16,
          fontWeight: FontWeight.w700,
          text: 'Logout',
          padding: 14,
          onPressed: () {
            AppAlertDialog(
              title: "Logout",
              description: "Are you sure you want to exit the application?",
              negativeButtonText: 'no',
              negativeButtonOnTap: () => Navigator.pop(context),
              positiveButtonText: 'yes',
              positiveButtonOnTap: () {
                App().prefs!.setBool(ConstantHelper.PREFS_IS_USER_LOGGED_IN, false);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const LoginPage()),
                        (Route<dynamic> route) => false);
              },
            ).show(context);
          },
        )
      ),
    );
  }
}
