import 'dart:ui';
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  final String? title;
  final FontWeight? titleFontWeight;
  final String? placeholder;
  final TextEditingController controller;
  final bool autoFocus;
  final Widget? suffixTitle;
  final GestureTapCallback? onTap;
  final Widget? prefixIcon;
  final Function(String)? onChanged;
  final bool clearable;
  final String? error;
  final Color background;
  final bool required;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final double? size;
  final List<TextInputFormatter>? inputFormatters;
  final bool obscureText;
  final bool readOnly;
  final List<FontFeature>? fontFeatures;
  final double radius;
  final BoxConstraints? prefixIconConstraints;
  final int? maxLines;
  final FocusNode? focusNode;
  final Color? borderColor;
  final bool? isElevation;
  final int? maxLength;
  final bool? withErrorWidget;
  final FormFieldValidator<String>? validator;


  const CustomTextField({
    Key? key,
    this.title,
    this.titleFontWeight = FontWeight.w500,
    this.placeholder,
    required this.controller,
    this.autoFocus = false,
    this.suffixTitle,
    this.onTap,
    this.clearable = false,
    this.prefixIcon,
    this.onChanged,
    this.error,
    this.background = const Color(0xFFFFFFFF),
    this.required = false,
    this.keyboardType,
    this.textInputAction = TextInputAction.done,
    this.size,
    this.inputFormatters,
    this.obscureText = false,
    this.readOnly = false,
    this.fontFeatures,
    this.radius = 12,
    this.prefixIconConstraints,
    this.maxLines,
    this.focusNode,
    this.borderColor,
    this.isElevation = false,
    this.maxLength,
    this.withErrorWidget = true,
    this.validator,
  }) : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.title?.isNotEmpty == true
            ? Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Text(widget.title ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                fontWeight: widget.titleFontWeight,
                                color: Colors.black,
                                fontSize: 14)),
                      ),
                      widget.suffixTitle ?? Container()
                    ],
                  ),
                  const SizedBox(height: 8),
                ],
              )
            : Container(),
        Container(
          decoration: BoxDecoration(
              boxShadow: [
                widget.isElevation == true
                    ? BoxShadow(
                        color: Colors.grey.withOpacity(0.4),
                        spreadRadius: 2,
                        blurRadius: 8,
                      )
                    : const BoxShadow(
                        color: Colors.transparent,
                        offset: Offset(0.0, 0.0),
                        blurRadius: 0.0,
                      )
              ],
              color: widget.background,
              border: Border.all(
                  color: widget.error == null || widget.error?.isEmpty == true
                      ? (widget.borderColor != null
                          ? widget.borderColor!
                          : AbersoftColors.colorPrimary)
                      : AbersoftColors.colorRed),
              borderRadius: BorderRadius.circular(widget.radius)),
          child: TextFormField(
            textAlignVertical: TextAlignVertical.center,
            inputFormatters: widget.inputFormatters,
            controller: widget.controller,
            readOnly: widget.readOnly,
            enabled: !widget.readOnly,
            autofocus: widget.autoFocus,
            keyboardType: widget.keyboardType,
            onTap: widget.onTap,
            onChanged: widget.onChanged,
            maxLines: widget.maxLines,
            obscureText: widget.obscureText,
            cursorColor: Colors.black,
            style: TextStyle(
                fontSize: widget.size, fontFeatures: widget.fontFeatures),
            textInputAction: widget.textInputAction,
            validator: widget.validator ?? (String? args) => null,
            decoration: InputDecoration(
                counterText: '',
                contentPadding: const EdgeInsets.fromLTRB(8, 14, 8, 16),
                isDense: true,
                border: InputBorder.none,
                hintText: widget.placeholder,
                hintStyle: TextStyle(fontSize: widget.size, color: Colors.grey[300]),
                prefixIconConstraints: widget.prefixIconConstraints,
                prefixIcon: widget.prefixIcon != null
                    ? IconTheme(
                        child: widget.prefixIcon!,
                        data: const IconThemeData(color: AbersoftColors.colorDarkGrey))
                    : null,
                suffixIconConstraints:
                    const BoxConstraints(maxHeight: 25, maxWidth: 55),
                suffixIcon: widget.clearable && widget.controller.text.isNotEmpty
                    ? IconButton(
                        padding: EdgeInsets.zero,
                        onPressed: () {
                          widget.controller.text = "";
                          widget.onChanged?.call("");
                        },
                        icon: const Icon(Icons.close,
                            color: Colors.black, size: 25),
                      )
                    : null),
            focusNode: widget.focusNode,
            maxLength: widget.maxLength,
          ),
        ),
        widget.withErrorWidget == true
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 4),
                  widget.error != null && widget.error!.isNotEmpty
                      ? Text(widget.error!,
                          style: const TextStyle(
                              color: AbersoftColors.colorRed, fontSize: 11))
                      : Container(height: 16)
                ],
              )
            : Container(),
      ],
    );
  }
}
