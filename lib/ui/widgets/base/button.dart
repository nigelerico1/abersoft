import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


enum AppButtonStyle {
  primary,
  secondary,
  third,
  fourth
}

class Button extends StatelessWidget {
  final AppButtonStyle style;
  final String text;
  final bool isLoading;
  final bool isDisabled;
  final VoidCallback onPressed;
  final double fontSize;
  final FontWeight fontWeight;
  final double padding;
  final double radiusCircular;
  final double? height;
  final Color? colorsText;
  final IconData? faIcon;
  final Color? colorIcon;

  Button({
    this.style = AppButtonStyle.primary,
    this.text = '',
    required this.onPressed,
    this.isLoading = false,
    this.isDisabled = false,
    this.fontSize = 18,
    this.fontWeight = FontWeight.w500,
    required this.padding,
    this.radiusCircular = 6,
    this.height,
    this.colorsText = Colors.white,
    this.colorIcon = Colors.white,

    this.faIcon
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
          height: height,
          padding: EdgeInsets.symmetric(vertical: padding),
          decoration: BoxDecoration(
              color: _getButtonColorByStyle(style),
              borderRadius: BorderRadius.all(Radius.circular(radiusCircular))
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              faIcon != null ? Padding(
                padding: const EdgeInsets.symmetric(horizontal: 8.0),
                child: FaIcon(faIcon, color: colorIcon),
              ) : Container(),
              Center(
                child: Text(
                  text,
                  style: TextStyle(
                      fontSize: fontSize,
                      color: colorsText,
                      fontWeight: fontWeight
                  ),
                ),
              ),
            ],
          )
      ),
    );
  }

  Color _getButtonColorByStyle(AppButtonStyle style) {
    Color color;

    switch (style) {
      case AppButtonStyle.primary:
        color = AbersoftColors.colorPrimary;
        break;

      case AppButtonStyle.secondary:
        color = AbersoftColors.colorDarkGrey;
        break;

      case AppButtonStyle.third:
        color = AbersoftColors.colorOrange;
        break;

      case AppButtonStyle.fourth:
        color = AbersoftColors.colorDarkGrey;
        break;
    }



    return color;
  }
}
