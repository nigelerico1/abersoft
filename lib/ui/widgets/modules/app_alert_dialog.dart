
import 'package:abersoft/utils/AbersoftColors.dart';
import 'package:flutter/material.dart';

class AppAlertDialog {
  final String title;
  final String? description;
  final String? description2;
  final String? positiveButtonText;
  final Function? positiveButtonOnTap;

  final String? negativeButtonText;
  final Function? negativeButtonOnTap;
  final bool barrierDismissible;

  AppAlertDialog({
    required this.title,
    required this.description,
    this.positiveButtonText,
    this.description2 = "",
    this.positiveButtonOnTap,
    this.negativeButtonText,
    this.negativeButtonOnTap,
    this.barrierDismissible = true
  });

  void show(BuildContext context) {
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10)
            ),
            title: Text(title),
            content: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(description!),
                description2 != "" ? Padding(
                  padding: const EdgeInsets.only(top : 8.0),
                  child: Text(description2! ,style: const TextStyle(fontSize: 10, fontWeight: FontWeight.w400)),
                ) : Container(),
              ],
            ),
            actions: <Widget>[
              negativeButtonText != null && negativeButtonOnTap != null ?
              TextButton(
                child: Text(negativeButtonText!, style: const TextStyle(color: AbersoftColors.colorPrimary)),
                onPressed: () => negativeButtonOnTap!(),
              ) : Container(),
              positiveButtonText != null && positiveButtonOnTap != null ?
              TextButton(
                child: Text(positiveButtonText!, style: const TextStyle(color: AbersoftColors.colorPrimary)),
                onPressed: () => positiveButtonOnTap!(),
              ) : Container(),
            ],
          );
        }
    );
  }
}
