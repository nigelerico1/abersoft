import 'package:abersoft/app.dart';
import 'package:abersoft/ui/pages/auth/login_page.dart';
import 'package:abersoft/ui/pages/home/navigator_page.dart';
import 'package:abersoft/utils/constant_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1500)).then((_) async {
      bool isLoggedIn =
          App().prefs!.getBool(ConstantHelper.PREFS_IS_USER_LOGGED_IN) ?? false;
      if (isLoggedIn) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const NavigatorPage()),
                (Route<dynamic> route) => false);
      } else {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => const LoginPage()),
                (Route<dynamic> route) => false);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Center(
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
            ),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset('assets/images/LOGO.png',
                width: ScreenUtil().setWidth(200),
                height: ScreenUtil().setWidth(200)),
          ),
        ),
      ),
    );
  }
}
