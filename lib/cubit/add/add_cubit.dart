import 'package:abersoft/models/add/add_model.dart';
import 'package:abersoft/models/add/add_request.dart';
import 'package:abersoft/services/add_services.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'add_state.dart';

class AddCubit extends Cubit<AddState> {
  AddCubit() : super(AddInitial());

  final AddServices _addServices = AddServices();

  void addProduct(AddRequest data) async {
    emit(AddProductInitialState());

    AddModel apiResult = await _addServices.addProduct(data);
    if (apiResult.id != null) {

      emit(const AddProductSuccessfulState("Success Add Product"));
    } else {
      emit(const AddProductFailedState("Fail Add Product"));
    }
  }
}
