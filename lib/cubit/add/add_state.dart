part of 'add_cubit.dart';

abstract class AddState extends Equatable {
  const AddState();
  @override
  List<Object> get props => [];
}

class AddInitial extends AddState {}

class AddProductInitialState extends AddState {}

class AddProductSuccessfulState extends AddState {
  final String? message;

  const AddProductSuccessfulState(this.message);
}

class AddProductFailedState extends AddState {
  final String? message;

  const AddProductFailedState(this.message);
}

