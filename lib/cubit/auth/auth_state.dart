part of 'auth_cubit.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}


class LoginInitialState extends AuthState {}

class LoginSuccessfulState extends AuthState {
  final String? message;

  const LoginSuccessfulState(this.message);
}

class LoginFailedState extends AuthState {
  final String? message;

  const LoginFailedState(this.message);
}
