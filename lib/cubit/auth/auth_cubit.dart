import 'package:abersoft/app.dart';
import 'package:abersoft/models/auth/login_model.dart';
import 'package:abersoft/models/auth/login_request.dart';
import 'package:abersoft/services/auth_services.dart';
import 'package:abersoft/utils/constant_helper.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  final AuthService _authService = AuthService();

  void loginUser(LoginRequest data) async {
    emit(LoginInitialState());

    LoginModel apiResult = await _authService.login(data);
    if (apiResult.token != null || apiResult.token != '') {

      App().prefs!.setBool(ConstantHelper.PREFS_IS_USER_LOGGED_IN, true);
      App().dio!.options.headers = {
        'Authorization': 'Bearer ${apiResult.token}',
      };

      emit(LoginSuccessfulState(apiResult.token));
    } else {
      emit(LoginFailedState(apiResult.token));
    }
  }


}
