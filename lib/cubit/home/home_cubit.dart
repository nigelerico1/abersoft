import 'package:abersoft/models/product/product_model.dart';
import 'package:abersoft/services/home_services.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  HomeCubit() : super(HomeInitial());

  final HomeServices _homeServices = HomeServices();

  int selectedPage = 0;

  void changeSelectedPage(int targetPage) {
    emit(ChangeMainPageInit());
    selectedPage = targetPage;
    emit(ChangedMainPage());
  }

  void getListProducts() async {
    emit(GetProductsInitialState());

    ProductModel apiResult = await _homeServices.getProductList();

    if (apiResult.allProduct != null) {
      List<AllProduct>? bestProducts = apiResult.bestProduct;
      List<AllProduct>? allProducts = apiResult.allProduct;

      emit(GetProductsSuccessfulState("Success get data products", bestProducts ?? [], allProducts ?? []));
    } else {
      emit(const GetProductsFailedState("Fail get data products"));
    }
  }

}
