part of 'home_cubit.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class ChangeMainPageInit extends HomeState {}

class ChangedMainPage extends HomeState {}

class GetProductsInitialState extends HomeState {}

class GetProductsSuccessfulState extends HomeState {
  final String? message;
  final List<AllProduct> bestProducts;
  final List<AllProduct> allProducts;

  const GetProductsSuccessfulState(this.message, this.bestProducts, this.allProducts);
}

class GetProductsFailedState extends HomeState {
  final String? message;

  const GetProductsFailedState(this.message);
}

