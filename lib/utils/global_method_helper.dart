

class GlobalMethodHelper {
  static bool isEmpty(text){
    if(text == "" || text == null || text == "null"){
      return true;
    }else{
      return false;
    }
  }

  static bool isEmptyList(List<dynamic> list){
    if(list == null){
      return true;
    } else if (list.isEmpty){
      return true;
    }else{
      return false;
    }
  }

}