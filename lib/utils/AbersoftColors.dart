import 'package:flutter/material.dart';

class AbersoftColors {

  static const colorPrimary = Color(0xFF0389F7);
  static const colorPrimaryLight = Color(0xFFE8E8CC);
  static const colorWhite = Color(0xFFFFFFFF);
  static const colorBlack = Color(0xFF000000);
  static const colorYellow = Color(0xFFFFCC1D);
  static const colorBlackMenu = Color(0xFF585858);
  static const colorOrange = Color(0xFFFF725E);
  static const colorRedMaron = Color(0xFFFF6B6B);
  static const colorRed = Color(0xFFFF1111);
  static const colorPurple = Color(0xFFDBD8EF);
  static const colorDarkGrey = Color(0xFFD9D9D9);
  static const colorGrey = Color(0xFF7A7A7A);
  static const colorLightGrey = Color(0xFFF5F5F5);
  static const colorDarkGreyInbox = Color(0xFFDEDEDE);
  static const colorLightGreyInbox = Color(0xFFFBFBFB);
}