class ConstantHelper {

  static const String PREFS_TOKEN_KEY = 'PREFS_TOKEN_KEY';
  static const String PREFS_IS_USER_LOGGED_IN = 'PREFS_IS_USER_LOGGED_IN';
  static const String PREFS_USER_ID = 'PREFS_USER_ID';
  static const String PREFS_USER_NAME = 'PREFS_USER_NAME';
  static const String PREFS_USER_EMAIL = 'PREFS_USER_EMAIL';
  static const String PREFS_USER_ROLE = 'PREFS_USER_ROLE';
  static const String PREFS_FIREBASE_TOKEN = 'PREFS_FIREBASE_TOKEN';

  static const int RESPONSE_SUCCESS = 200;

  static const String CONTENT_TYPE = "application/json";

  static const String EXT_PDF = 'pdf';
  static const String EXT_IMAGE = 'image';
  static const String EXT_EXCEL = 'xlsx';


}