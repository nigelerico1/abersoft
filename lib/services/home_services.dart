import 'package:abersoft/app.dart';
import 'package:abersoft/models/product/product_model.dart';
import 'package:abersoft/utils/url_constant_helper.dart';
import 'package:dio/dio.dart';

class HomeServices {
  final Dio? _dio = App().dio;

  Future<ProductModel> getProductList() async {
    try {
      Response response = await _dio!.get(
          UrlConstantHelper.PRODUCTS,
      );
      if (response.statusCode == 200) {
        return ProductModel(
          bestProduct: (response.data['bestProduct'] as Iterable)
              .map((e) => AllProduct.fromJson(e))
              .toList(),
          allProduct: (response.data['allProduct'] as Iterable)
              .map((e) => AllProduct.fromJson(e))
              .toList(),
        );
      }

      return ProductModel(bestProduct: [], allProduct: []);
    } on DioError catch (e) {
      return ProductModel(
        bestProduct: e.response?.data['bestProduct'],
        allProduct: e.response?.data['allProduct'],
      );
    }
  }
}