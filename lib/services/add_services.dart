import 'package:abersoft/app.dart';
import 'package:abersoft/models/add/add_model.dart';
import 'package:abersoft/models/add/add_request.dart';
import 'package:abersoft/utils/url_constant_helper.dart';
import 'package:dio/dio.dart';

class AddServices {
  final Dio? _dio = App().dio;

  Future<AddModel> addProduct(AddRequest data) async {
    try {
      Response response = await _dio!.post(
          UrlConstantHelper.PRODUCTS,
          data: FormData.fromMap(await data.toJson()),
      );
      if (response.statusCode == 200) {
        return AddModel(
          id: response.data['id'],
          name: response.data['name'],
          imageUrl: response.data['imageUrl'],
          productDescription: response.data['productDescription'],
        );
      }

      return AddModel();
    } on DioError catch (e) {
      return AddModel(
        id: e.response?.data['id'],
        name: e.response?.data['name'],
        imageUrl: e.response?.data['imageUrl'],
        productDescription: e.response?.data['productDescription'],
      );
    }
  }
}