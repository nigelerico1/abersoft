import 'package:abersoft/app.dart';
import 'package:abersoft/models/auth/login_model.dart';
import 'package:abersoft/models/auth/login_request.dart';
import 'package:abersoft/utils/url_constant_helper.dart';
import 'package:dio/dio.dart';

class AuthService {
  final Dio? _dio = App().dio;

  Future<LoginModel> login(LoginRequest data) async {
    try {
      Response response = await _dio!.post(
          UrlConstantHelper.POST_AUTH_LOGIN,
          data: data.toJson()
      );
      if (response.statusCode == 200) {
        return LoginModel(
          token: response.data['token'],
        );
      }

      return LoginModel(token: '',);
    } on DioError catch (e) {
      return LoginModel(
        token: e.response?.data['token'],
      );
    }
  }
}