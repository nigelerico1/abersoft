import 'dart:io';
import 'package:abersoft/app.dart';
import 'package:abersoft/cubit/bloc_observer.dart';
import 'package:abersoft/main_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = MyBlocObserver();

  App.configure(
      apiBaseURL: 'https://2e3d13cc-3d6d-4911-b94c-ba23cf332933.mock.pstmn.io/api/',
      appTitle: 'Abersoft Technologies'
  );

  await App().init();

  runApp(const MainApp());
}

