import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';

class AddRequest {
  String? productName;
  XFile? productImage;
  String? productDecription;

  AddRequest(
      {this.productName,
        this.productImage,
        this.productDecription
      });

  AddRequest.fromJson(Map<String, dynamic> json) {
    productName = json['productName'];
    productImage = json['productImage'];
    productDecription = json['productDecription'];
  }

  Future<Map<String, dynamic>> toJson() async {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['productName'] = productName;
    data['productImage'] = productImage != null ? await MultipartFile.fromFile(
        productImage!.path, filename: productImage!.path.split('/').last
    ) : null;
    data['productDecription'] = productDecription;
    return data;
  }


}