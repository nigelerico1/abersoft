class AddModel {
  int? id;
  String? name;
  String? imageUrl;
  String? productDescription;

  AddModel({this.id, this.name, this.imageUrl, this.productDescription});

  AddModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['imageUrl'];
    productDescription = json['productDescription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['imageUrl'] = imageUrl;
    data['productDescription'] = productDescription;
    return data;
  }
}