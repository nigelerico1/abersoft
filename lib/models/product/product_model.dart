class ProductModel {
  List<AllProduct>? bestProduct;
  List<AllProduct>? allProduct;

  ProductModel({this.bestProduct, this.allProduct});

  ProductModel.fromJson(Map<String, dynamic> json) {
    if (json['bestProduct'] != null) {
      bestProduct = <AllProduct>[];
      json['bestProduct'].forEach((v) {
        bestProduct!.add(AllProduct.fromJson(v));
      });
    }
    if (json['allProduct'] != null) {
      allProduct = <AllProduct>[];
      json['allProduct'].forEach((v) {
        allProduct!.add(AllProduct.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (bestProduct != null) {
      data['bestProduct'] = bestProduct!.map((v) => v.toJson()).toList();
    }
    if (allProduct != null) {
      data['allProduct'] = allProduct!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class AllProduct {
  int? id;
  String? name;
  String? imageUrl;
  String? productDescription;

  AllProduct({this.id, this.name, this.imageUrl, this.productDescription});

  AllProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['imageUrl'];
    productDescription = json['productDescription'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['imageUrl'] = imageUrl;
    data['productDescription'] = productDescription;
    return data;
  }
}